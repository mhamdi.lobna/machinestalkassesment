@tag
Feature: Register to SonarQube
  As a user i want to create a free account on the SonarCube website

  @submit
  Scenario: submit a form
    Given go to the url "/automation-practice-form"
    When I Fill the form with valid data
    And I Submit the form
    Then I Validate that the entered data meet the displayed data

