package org.e2etests.automation.page_object;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;


import org.e2etests.automation.utils.ConfigFileReader;
import org.e2etests.automation.utils.Setup;

public class AddDataPage {
	private ConfigFileReader configFileReader;
	
	final static String FIRSTNAME = "firstName";
	final static String LASTNAME = "lastName";
	final static String EMAIL = "userEmail";
	final static String GENDER = "//label[@for='gender-radio-2']";
	final static String PHONE = "userNumber";
	final static String BIRTH = "Choose Friday, April 13th, 1984";
	final static String SUBJECTS = "//input[@id='subjectsInput']";
	final static String PICTURE = "uploadPicture";
	final static String SUBMIT = "submit";
	final static String CITY = "//*[contains(test(),'NCR']";
	final static String STATE = "//*[contains(test(),'DELHI']";
	final static String HOBBIES = "hobbies-checkbox-2";
	final static String ADRESS = "currentAddress";
	
	/* @FindBy */
	@FindBy(how = How.ID, using = FIRSTNAME)
	public static WebElement firstName;
	@FindBy(how = How.ID, using = LASTNAME)
	public static WebElement lastName;
	@FindBy(how = How.ID, using = EMAIL)
	public static WebElement email;
	@FindBy(how = How.XPATH, using = GENDER)
	public static WebElement gender;
	@FindBy(how = How.ID, using = PHONE)
	public static WebElement phone;
	@FindBy(how = How.XPATH, using = BIRTH)
	public static WebElement birth;
	@FindBy(how = How.XPATH, using = SUBJECTS)
	public static WebElement subjects;
	@FindBy(how = How.ID, using = PICTURE)
	public static WebElement picture;
	@FindBy(how = How.ID, using = SUBMIT)
	public static WebElement btnSubmit;
	@FindBy(how = How.ID, using = CITY )
	public static WebElement city;
	@FindBy(how = How.ID, using = STATE)
	public static WebElement state;
	@FindBy(how = How.ID, using = ADRESS)
	public static WebElement adress;
	@FindBy(how = How.ID, using = HOBBIES)
	public static WebElement hobbies;
	
	public AddDataPage() {
		PageFactory.initElements(Setup.getDriver(), this);
		this.configFileReader = new ConfigFileReader();
	}

	public void goToUrl(String page) throws InterruptedException {
		Setup.getDriver().get(configFileReader.getProperties("url.home")
			+page);
		Thread.sleep(3000);
	}
	
	public void FillInput(WebElement e, String value) {
		//e.clear();
		e.sendKeys(value);
	}
	public void clickOnButton(WebElement e) {
		e.click();
	}
	
	public void uploadFile(String path) {
		StringSelection stringSelection = new StringSelection(path);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, null);

		Robot robot = null;
		try {
			robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}

		robot.delay(250);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(150);
		robot.keyRelease(KeyEvent.VK_ENTER);

	}
}
