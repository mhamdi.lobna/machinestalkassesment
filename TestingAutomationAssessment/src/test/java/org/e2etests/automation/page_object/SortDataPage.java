package org.e2etests.automation.page_object;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.e2etests.automation.utils.ConfigFileReader;
import org.e2etests.automation.utils.Setup;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SortDataPage {

	private ConfigFileReader configFileReader;
	public SortDataPage() {
		PageFactory.initElements(Setup.getDriver(), this);
		this.configFileReader = new ConfigFileReader();
	}

	public void goToUrl() {
		Setup.getDriver().get(configFileReader.getProperties("url.home") + configFileReader.getProperties("page2"));
	}

	public void SortElement() {
		//List<String> numbers = Arrays.asList("Six", "Five", "Four", "Three", "Two", "One");
		Actions action = new Actions(Setup.getDriver());
		for (int i=6;i>1;i--) {
			String sid ="#demo-tabpane-list > div > div:nth-child("+i+")";
			String did ="#demo-tabpane-list > div > div:nth-child("+(7-i)+")";
			WebElement source = Setup.getDriver().findElement(By.cssSelector(sid));
			WebElement destination = Setup.getDriver().findElement(By.cssSelector(did));
			action.dragAndDrop(source, destination);
		}
	}

}
