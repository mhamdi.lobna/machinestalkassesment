package org.e2etests.automation.step_definition;

import org.e2etests.automation.page_object.SortDataPage;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SortDataStepDefinition {
private SortDataPage sortDataPage;
	public SortDataStepDefinition() {
		sortDataPage = new SortDataPage();
	}
	

	@When("Sort the list of elements in descending order")
	public void sortTheListOfElementsInDescendingOrder() {
		sortDataPage.SortElement();
	}
	@Then("Validate that the list is sorted in a descending way")
	public void validateThatTheListIsSortedInADescendingWay() {
	    
	}



}
