package org.e2etests.automation.step_definition;


import org.e2etests.automation.page_object.AddDataPage;
import org.e2etests.automation.utils.ConfigFileReader;
import org.e2etests.automation.utils.Setup;
import org.junit.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;



public class AddDataStepDefinition {
	private ConfigFileReader configFileReader;
	private AddDataPage addDataPage;
	public AddDataStepDefinition() {
		addDataPage = new AddDataPage();
		this.configFileReader = new ConfigFileReader();
	}
	
	@Given("go to the url {string}")
	public void goToTheUrl(String s) throws InterruptedException {
		addDataPage.goToUrl(s);
	}
	@When("I Fill the form with valid data")
	public void iFillTheFormWithValidData()  {
		addDataPage.FillInput(addDataPage.firstName, configFileReader.getProperties("first.name"));
		addDataPage.FillInput(addDataPage.lastName, configFileReader.getProperties("last.name"));
		addDataPage.FillInput(addDataPage.email, configFileReader.getProperties("email"));
		addDataPage.FillInput(addDataPage.phone, configFileReader.getProperties("phone"));
		addDataPage.FillInput(addDataPage.subjects, configFileReader.getProperties("subjects"));
		addDataPage.FillInput(addDataPage.hobbies, configFileReader.getProperties("hobbies"));
		addDataPage.FillInput(addDataPage.adress, configFileReader.getProperties("adress"));
		addDataPage.clickOnButton(addDataPage.gender);
		addDataPage.clickOnButton(addDataPage.birth);
		addDataPage.clickOnButton(addDataPage.state);
		addDataPage.clickOnButton(addDataPage.city);
		addDataPage.uploadFile(configFileReader.getProperties("path"));
		
	}
	@When("I Submit the form")
	public void iSubmitTheForm() {
		addDataPage.clickOnButton(addDataPage.btnSubmit);
		
	}
	@Then("I Validate that the entered data meet the displayed data")
	public void iValidateThatTheEnteredDataMeetTheDisplayedData() {
		Assert.assertEquals(Setup.getDriver().getPageSource(), "Lobna");
	}



}
