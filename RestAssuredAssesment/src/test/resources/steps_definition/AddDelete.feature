@tag
Feature: Add and Delete a user to the users list

@add_user
  Scenario: Add a user to the users list
    Given a list of users are available
    When I add a user to my list "new name" , "17", 11
    Then get the list of users after add
  
    
@delete_user
 Scenario: Delete the created user
    Given a list of users are available 
    When delete th user with id 1
    Then  get the list of users after deleting without id
    
    