package steps_definition;

import java.util.List;
import java.util.Map;

import org.junit.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import static io.restassured.response.Response.*;
//import io.restassured.RestAssured.*;
import static io.restassured.RestAssured.given;

import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class AddDeleteStepDefinition {

	private Response response;
	private ValidatableResponse json;
	private RequestSpecification request;
	private String baseUrl = "https://6355251c483f5d2df3adf4b9.mockapi.io";
	private String jsonString;

	@Given("a list of users are available")
	public void a_list_of_users_are_available() {
		response = given()
				.get(baseUrl+"/Users")
				.then()
				.contentType(ContentType.JSON)
				.extract()
				.response();
		jsonString = response.asString();
		Assert.assertTrue(jsonString.length() > 0);
		// System.out.println(response.asString());
	}

	@When("I add a user to my list {string} , {string}, {int}")
	public void i_add_a_user_to_my_list(String name, String age, int id) {


		String jsonBody = "\"name\": \"" + name + ", \"age\": \"" + age + ", \"id\": \"" + id + '}';

		response =given()			
				.accept(ContentType.JSON)
				.contentType(ContentType.JSON)
				.and()
				.body(jsonBody)
				.when()
				.post(baseUrl + "/Users/");
		
		
	}

	@Then("get the list of users after add")
	public void get_the_list_of_users_after_add() {
		Assert.assertEquals(201, response.getStatusCode());

	}

	@When("delete th user with id {int}")
	public void delete_th_user_with_id(Integer id) {
		String jsonBody = "{\"name\":\"name 1\",\"age\":42,\"id\":\"1\"}";

		response =given()			
		.header("Content-Type", "application/json")
		.body(jsonBody)
		.delete(baseUrl+"/Users/");



	}

	@Then("get the list of users after deleting without id")
	public void get_the_list_of_users_after_deleting() {
		Assert.assertEquals(200, response.getStatusCode());
		
		
	}

}
